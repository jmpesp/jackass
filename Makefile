BIN=jackass
CFLAGS= -g
all:$(BIN)
$(BIN): main.c hashtab.c
	$(CC) $(CFLAGS) -o $@ $^
clean:
	$(RM) $(BIN)
install:
	cp -p $(BIN) /usr/bin/$(BIN)
