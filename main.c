#include "hashtab.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/* <syntax>
 * PROC(root)	:STATEMENT '\n' PROC
 * 				|nil
 *
 * STATEMENT	:'def' name
 * 				|'set' name EXP
 * 				|'print' name
 * 				|nil
 *
 * EXP			:integer
 * 				|integer '+' EXP
 * 				|name
 * 				|name '+' EXP
 */
typedef enum {INTEGER, NAME, PLUS, DEF, SET, PRINT, ENDLINE} class_t;

typedef struct {
	class_t class;
	union {int Integer; char Name[32]; } value;
} word_t;

static int line = 1;
static int column = 1;
FILE * fd;
HTable __vartable;


static void prerror() {
	printf("syntax error: line %d column %d\n", line, column);
	exit(-1);
}
static int readword(word_t* word,int peek) {
	char i=1,n=1,p=1,lf=1,idx,shift;
	char buf[sizeof(word_t)];
	shift = 0, idx = 0;

	for(;;){
		buf[idx] = fgetc(fd);

		if(!peek) column ++;

		if(buf[idx] == EOF) {
			if(idx == 0) {
				return -1;
			} else {
				if(n==1 || i==1) {
					idx ++;
					break;
				}
			}
		}
		if(buf[idx]>='0' && buf[idx]<='9') {
			if(idx == 0)
				n=0,p=0,lf=0;
			else if(n == 1)
				i=0,p=0,lf=0;
		} else if(buf[idx]>='a' && buf[idx]<='z' || buf[idx]>='A' && buf[idx]<='Z') {
			if(idx == 0) {
				i=0,p=0,lf=0;
			} else if(i == 1) {
				prerror();
				return 1;
			}
		} else if(buf[idx] == '+') {
			if(idx == 0) {
				i=0,n=0,lf=0;
				idx ++;
				break;
			} else {
				if(i==1 || n==1) {
					ungetc(buf[idx], fd);
					if(!peek) column ++;
					//idx ++;
					break;
				}
				
			}
		} else if(buf[idx] == ' ' || buf[idx] == '\t') {
			if(idx == 0) {
				//left ++;
				shift ++;
				continue;
			} else if(n==1 || i==1) {
				if(peek) ungetc(buf[idx], fd);
				break;
			}
		} else if(buf[idx] == '\n') {
			if(idx == 0) {
				n=0,i=0,p=0;
				idx ++;
				if(!peek) line ++,column = 1;
				break;
			} else {
				if(n==1 || i==1) {
					ungetc(buf[idx], fd);
					break;
				}
				if(!peek) line ++,column = 1;
			}

		} else {
			prerror();
			return 1;
		}
		idx ++;
	}

	buf[idx] = 0;
	if(n) {
		if(strcmp(buf, "def") == 0)
			word->class = DEF;
		else if(strcmp(buf, "set") == 0)
			word->class = SET;
		else if(strcmp(buf, "print") == 0)
			word->class = PRINT;
		else {
			word->class = NAME;
			strcpy(word->value.Name, buf);
		}
	} else if(i) {
		word->class = INTEGER;
		sscanf(buf, "%d", &word->value.Integer);
	} else if(p) {
			word->class = PLUS;
	} else if(lf) {
			word->class = ENDLINE;
	} else {
		fprintf(stderr, "Internal error.\n");
		exit(1);
	}
	if(peek) {
		while(shift --> 0) {
			ungetc(' ',fd);
		}
		while(idx --> 0) {
			ungetc(buf[idx],fd);
		}
	}

	return 0;
}

static int exp1() {
	word_t word, word1;
	int res;
	if((res=readword(&word,0)) == -1) prerror();
	if(word.class == INTEGER) {
		if((res=readword(&word1,1)) == -1) {
			return word.value.Integer;
		}

		if(word1.class != PLUS)
			return word.value.Integer;

		readword(&word1,0);

		return word.value.Integer + exp1();
	} else if(word.class == NAME) {
		int *val;
		res=readword(&word1,1);
		if(res == -1 || word1.class != PLUS) {
			if(!htable_get(&__vartable, word.value.Name, (void**)&val)) {
				fprintf(stderr, "symbol `%s' not found\n", word.value.Name);
				exit(-1);
			}
			if(val == NULL) {
				fprintf(stderr, "varient `%s' not initialized\n", word.value.Name);
				exit(-1);
			}
			return *val;
		}
			
		readword(&word1,0);
		if(!htable_get(&__vartable, word.value.Name, (void**)&val)) {
			fprintf(stderr, "symbol `%s' not found\n", word.value.Name);
			exit(-1);
		}
		if(val == NULL) {
			fprintf(stderr, "varient `%s' not initialized\n", word.value.Name);
			exit(-1);
		}
		return *val + exp1();
	}
}

static void statement() {
	word_t word,word1;
	int res;

	if((res=readword(&word,1)) == -1) return;
	if(word.class == ENDLINE)
		return;

	readword(&word,0);

	switch(word.class) {
		case DEF:
			if((res=readword(&word,0)) == -1) prerror();
			if(word.class != NAME)
				prerror();
			if(htable_exists(&__vartable, word.value.Name)) {
				fprintf(stderr, "symbol `%s' already defined\n", word.value.Name);
				exit(-1);
			}
			htable_set(&__vartable, word.value.Name, NULL);
			break;
		case SET:
			if((res=readword(&word,0)) == -1) prerror();
			if(word.class != NAME)
				prerror();
			if(!htable_exists(&__vartable, word.value.Name)) {
				fprintf(stderr, "symbol `%s' not found\n", word.value.Name);
				exit(-1);
			} else {
				int *val;

				val = malloc(sizeof(int));
				if(val == NULL) 
					perror("malloc");
				*val = exp1();
				htable_set(&__vartable, word.value.Name, val);
			}
			break;
		case PRINT:
			if((res=readword(&word,0)) == -1) prerror();
			if(word.class != NAME)
				prerror();
			else {
				int *val;
				if(!htable_get(&__vartable, word.value.Name, (void**)&val)) {
					fprintf(stderr, "symbol `%s' not found", word.value.Name);
					exit(-1);
				}
				if(val)
					printf("%d\n", *val);
				else
					puts("NIL");
			}
			break;
		default:
			prerror();
	}

}
static void proc() {
	word_t word;
	int res;
	while(1) {
		statement();
		res = readword(&word,0);
		if(res == -1)
			break;
		else if(word.class != ENDLINE) {
			//printf("word.class : %d\n", word.class);
			prerror();
		}
	}
}

static void clean_vartable(void *v) {
		if(v) free(v);
}

static void usage() {
	printf("Jackass Script Interpreter\n" 
			"\tjackass [-h] [scriptfile]\n\n");
	exit(0);
}

int main(int argc, char *argv[]) {
#define LEXER_TEST 0
#if LEXER_TEST
	word_t word;
	fd = stdin;
	while(readword(&word,0) == 0) {
		printf("%d ", word.class);
		if(word.class == NAME) {
			printf("<%s>", word.value.Name);
		}
	}
	printf("\n");
#else
	if(argc == 1)
		fd = stdin;
	else if(argc == 2) {
		if(strcmp(argv[1], "-h") == 0)
			usage();
		else {
			if((fd = fopen(argv[1], "r")) == NULL)
				perror("fopen");
		}
	} else usage();
	htable_create(256,&__vartable,clean_vartable);
	proc();
#endif
	return 0;
}
